const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();
const app = express();

// Import Routes
const authRoute = require('./routes/auth');
const postRoute = require('./routes/posts');

//Conect to DB
const MONGOURI = 'mongodb://localhost:27017/mydb3';

mongoose.connect(MONGOURI, {useNewUrlParser:true}, () =>
console.log('connected to DB')
);

//Middleware
app.use(express.json());

//Route Middlewares
app.use('/api/user',authRoute);
app.use('/api/posts',postRoute);


app.listen(4000, () => console.log('Server Up and running'));